#!/usr/bin/env python3

# todo:
#



import numpy as np


def create_scaled_position_grid(shape):
    nsx = shape[0]
    nsy = shape[1]
    nsz = shape[2]

    sx = np.linspace(0, 1, nsx, endpoint=False)
    sy = np.linspace(0, 1, nsy, endpoint=False)
    sz = np.linspace(0, 1, nsz, endpoint=False)

    # not the correct indexing
    #scaled_position_grid = np.meshgrid(sx,sy,sz)

    scaled_position_grid = np.zeros((nsx,nsy,nsz, 3))
    Sx, Sy, Sz = np.meshgrid(sx,sy,sz, indexing = 'ij')
    scaled_position_grid[:,:,:,0] = Sx
    scaled_position_grid[:,:,:,1] = Sy
    scaled_position_grid[:,:,:,2] = Sz

    return scaled_position_grid

def create_position_grid(shape, cell):
    scaled_position_grid = create_scaled_position_grid(shape)
    position_grid = np.dot(scaled_position_grid, cell)
    return position_grid


def create_mic_distance_grid(shape, cell, position):
    scaled_position = cell.scaled_positions(np.array([position]))
    
    #print(scaled_position)
    #nsx = shape[0]
    #nsy = shape[1]
    #nsz = shape[2]

    scaled_position_grid = create_scaled_position_grid(shape)

    #scaled_distance_grid = np.zeros((nsx,nsy,nsz, 3))

    scaled_distance_grid =  scaled_position_grid - scaled_position
    scaled_distance_grid =  scaled_distance_grid - np.round(scaled_distance_grid, 0)

    distance_grid = np.dot(scaled_distance_grid, cell)
    return distance_grid


def create_mic_radius_grid(shape, cell, position):
    distance_grid = create_mic_distance_grid(shape = shape, cell = cell, position = position)
    radial_distance_grid = np.linalg.norm(distance_grid, axis = 3)

    return radial_distance_grid


def compute_cutoff_by_percent(rho,percent = 95):
    '''Works with complex valued field by using the absolute value'''

    threshold = percent * 0.01 * np.absolute(rho).sum()
    sorted_values = np.sort(rho,axis=None)

    sum_inside = 0.0
    i = sorted_values.size-1 #counts down
    while sum_inside<=threshold and i >= 0:
        #print sum_inside, threshold, i
        sum_inside += np.absolute(sorted_values[i])
        i -= 1
    cutoff_found = sorted_values[i+1]

    return cutoff_found



def wrap_data(rho, scaled_wrap_vector = None, wrap_vector=None, cell = None, coarse_wrapping = False ):

    swvc = np.zeros(3)
    if scaled_wrap_vector is not None:
        swvc = swvc + np.array(scaled_wrap_vector)

    if wrap_vector is not None:
        swvc = swvc + cell.scaled_positions(np.array([wrap_vector]))[0]
        # the matrix-y way incase ase implements a non-zero Cell origin
        #swvc = swvc + np.dot(wrap_vector, cell.reciprocal())

    #print(swvc)
    if coarse_wrapping == True:
        int_wrap = (swvc * np.array(rho.shape)).astype(int)
        rho_out = np.roll(rho, shift = int_wrap , axis = (0,1,2) )
    else:
        #fourier interpolation for sub grid shifts
        n = rho.shape
        k_rho = np.fft.fftn(rho)
        gshift =  swvc*np.array(rho.shape) # grid shift
        kx, ky, kz = np.meshgrid(np.fft.fftfreq(n[0]), np.fft.fftfreq(n[1]), np.fft.fftfreq(n[2]), indexing = 'ij' )
        phase_shift = np.exp(-2.0j*np.pi * ( kx*gshift[0] + ky*gshift[1] + kz*gshift[2]  ))
        rho_out = np.fft.ifftn(k_rho*phase_shift)

        if np.iscomplexobj(rho) ==False:
            rho_out = rho_out.real

    return rho_out


def oversample_data(rho, oversample = 1):
    if oversample == 1:
        return rho
    else:
        k_rho = np.fft.fftshift( np.fft.fftn(rho))
        new_dimensions = ( oversample* np.array(k_rho.shape) ).astype(int)
        #new_dimensions = (1024,1024,1024)
        print (k_rho.shape, 'Sampled to', new_dimensions)
        k_rho_oversample = np.zeros(new_dimensions, dtype = complex)
        lb = [0,0,0]
        ub = [0,0,0]

        if oversample >= 1.0:
            for i in range(3):
                lb[i] = k_rho_oversample.shape[i]//2 - k_rho.shape[i]//2
                ub[i] = lb[i] + k_rho.shape[i]
            k_rho_oversample[lb[0]:ub[0], lb[1]:ub[1], lb[2]:ub[2]] = k_rho

        elif oversample < 1.0:
            for i in range(3):
                lb[i] = k_rho.shape[i]//2 - k_rho_oversample.shape[i]//2
                ub[i] = lb[i] + k_rho_oversample.shape[i]
            k_rho_oversample = k_rho[lb[0]:ub[0], lb[1]:ub[1], lb[2]:ub[2]]

        k_rho_oversample = np.fft.ifftshift(k_rho_oversample)
        rho_out = np.fft.ifftn(k_rho_oversample)
        rho_out = ((1.0*rho_out.size)/k_rho.size)*rho_out

        if np.iscomplexobj(rho) == False:
            rho_out = rho_out.real

        return rho_out




def atomic_orbital_real(center, position_grid, n, l, m, bohr_radius):
    '''periodicity not implemented'''
    from scipy.special import sph_harm

    centered_grid = position_grid - center


    rho_sqr = centered_grid[...,0]**2 + centered_grid[...,1]**2
    r_sqr = rho_sqr + centered_grid[...,2]**2

    r_xy = np.sqrt(rho_sqr)
    r   = np.sqrt(r_sqr)

    theta = np.arctan2(centered_grid[...,1], centered_grid[...,0])
    phi = np.arctan2(centered_grid[...,2], r_xy) + np.pi/2 # not obvious shift

    rho = 2*r/(n*bohr_radius)# normalization

    if m < 0:
        Ylm = (1.0j/np.sqrt(2)) * (sph_harm(m,l,theta,phi) - ((-1)**m) * sph_harm(-m,l,theta,phi))
    elif m > 0:
        Ylm = (1.0/np.sqrt(2)) * (sph_harm(-m,l,theta,phi) + ((-1)**m) * sph_harm(m,l,theta,phi))
    else:
        Ylm = sph_harm(m,l,theta,phi)

    #Ylm = Ylm.real
    #Ylm = np.absolute(Ylm).real

    from scipy.special import eval_genlaguerre
    from math import factorial
    coeff = np.sqrt( (2/(n*bohr_radius))**3) * np.sqrt( factorial(n-l-1) / (2*n*factorial(n+l)) )

    Rnl = coeff * np.exp(-rho/2) * (rho**l) * eval_genlaguerre(n-l-1, 2*l+1, rho)

    orbital = Rnl*Ylm
    return orbital.real
