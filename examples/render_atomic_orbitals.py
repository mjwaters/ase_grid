

 # quantum number, 4 will get you the first f-shell
nmax = 4

# number of grid points to use for isosurface
ngrid = 64

# how tall the picture should be in pixels
picture_height = 2048

# box size, increase if states are clipped off 
L = 35 

# Camera rotations, check with ASE GUI on for which to use
#rotation = '24x, 34y, 14z'
#rotation = '-90x, 0y, 0z'
#rotation = '0x, 45y, -25z'
#rotation = '-90x, -45y, 54.7356103z'
rotation = '-82x, -8y, -1z'

# percent of the state to enclose
density_percent = 50 


run_povray = True
delete_pov_files = True
radius_scale = 0.5 # for the covalent_radii 
add_surfaces = True
closed_edges = False
positive_color = (0.2, 0.3, 0.8, .3)
negative_color = (0.7, 0.2, 0.2, .3)
#########################

import numpy as np
from ase import io, Atoms
from ase.io.pov import add_isosurface_to_pov
import os




texture_fancy='''\n\ttexture{
\n\t\tpigment {  rgbft  <%f, %f, %f, 0.1, 0.60> }
\n\t\tfinish{ diffuse 0.85 ambient 0.99 brilliance 3 specular 0.5 roughness 0.001
\n\t\treflection { .05,.98 fresnel on exponent 1.5 }
\n\t\tconserve_energy}
\n\t}
\n\t\tinterior { ior 1.3 }
\n\tphotons {
\n\t\ttarget
\n\t\trefraction on
\n\t\treflection on
\n\t\tcollect on
\n\t}'''




atoms =  Atoms(
            'H3',
            positions=[[L/2, L/2, L/2],[0,0,0], [L,L, L]],
            cell=np.full((3), L),
            pbc=[1, 1, 1])




from ase.data import atomic_numbers, covalent_radii
radius_list = []
for atomic_number in atoms.get_atomic_numbers():
    radius_list.append(radius_scale*covalent_radii[atomic_number])

kwargs = { # For povray files only
'display'      : False, # Display while rendering
'pause'        : False, # Pause when done rendering (only if display)
'transparent'  : True, # Transparent background
'canvas_width' : None,  # Width of canvas in pixels
'canvas_height': picture_height,  # Height of canvas in pixels
'show_unit_cell': 2, # this is broken in some versions
#'camera_dist'  : 19.0,   # Distance from camera to front atom, 20 for spin flip, 23 for conserve
#'camera_type': 'orthographic angle 35',  # 'perspective angle 20'
'radii' : radius_list, #atoms.positions.shape[0]*[0.3],
'textures': len(atoms)*['ase3'],
# some more options:
#'image_plane'  : None,  # Distance from front atom to image plane
#                        # (focal depth for perspective)
#'camera_type'  : 'perspective', # perspective, ultra_wide_angle
#'point_lights' : [],             # [[loc1, color1], [loc2, color2],...]
#'area_light'   : [(2., 3., 40.) ,# location
#                  'White',       # color
#                  .7, .7, 3, 3], # width, height, Nlamps_x, Nlamps_y
#'background'   : 'White',        # color
'celllinewidth': 0.00, # Radius of the cylinders representing the cell
'depth_cueing':False}




kwargs.update({'rotation': rotation})


from ase_grid import atomic_orbital_real, compute_cutoff_by_percent, create_position_grid

#position_grid = create_position_grid(shape = (80,100,120), cell = atoms.get_cell())

position_grid = create_position_grid(shape = 3*[ngrid], cell = atoms.get_cell())

#print(position_grid)
#print(position_grid[1,4,2])


import os


for n in range(1,nmax+1):
    for l in range(0,n):
        for m in range(-l,l+1):

            pov_name = 'orbital_n_%i_l_%i_m_%i.pov' % (n,l,m)
            ini_name = pov_name.replace('.pov', '.ini')
            print(pov_name)

            orbital = atomic_orbital_real(center = atoms.get_positions()[0],
                position_grid = position_grid,
                n = n, l = l, m = m,
                bohr_radius = 0.53) # in Ang

            orbital_density = orbital.conjugate() * orbital
            print('    norm', orbital_density.sum()*atoms.get_volume()/orbital_density.size )

            density_cut_off = compute_cutoff_by_percent(orbital_density, percent = density_percent)
            wfn_cut_off = np.sqrt(density_cut_off)
            print('    wfn cut', wfn_cut_off , 'density cut', density_cut_off)

            extras=[]

            from ase.io.pov import add_isosurface_to_pov
            #### charge density, defaults to a semitranslucent style solid style
            extras.append(( add_isosurface_to_pov,
                            dict( density_grid = orbital, cut_off = wfn_cut_off,
                            closed_edges =closed_edges,
                            color = positive_color, material = 'vmd' # see styles for more options or use the next line to overide with a custom povray material/texture
                            #material = texture_fancy%(positive_color[0:3] )
                            )))

            if orbital.min() < -wfn_cut_off:
                extras.append(( add_isosurface_to_pov,
                                dict( density_grid = orbital, cut_off = -wfn_cut_off,
                                closed_edges =closed_edges,
                                color = negative_color, material = 'vmd'
                                #material = texture_fancy%(negative_color[0:3] )
                                )))


            kwargs['extras'] = extras
            io.write(pov_name, atoms, run_povray=run_povray, **kwargs)

            if delete_pov_files:
                os.remove(pov_name)
                os.remove(ini_name)
